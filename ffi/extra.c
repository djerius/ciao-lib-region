#include <ffi_platypus_bundle.h>
#include <cxcregion.h>
#include <region_priv.h>
#include <stdio.h>
#include "ffi/common.h"

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "ppport.h"

/*
 *  similar to regRegionToMask, but caller must allocate
 */
void PFX(RegionToMask)(regRegion * region,
                            long ixmin,
                            long ixmax,
                            long iymin,
                            long iymax,
                            double xc,
                            double yc,
                            double bin,
                            short *mask )
{
    long iy;
    for ( iy = iymin; iy <= iymax; ++iy ) {
        double y = yc + iy * bin;
        long ix;
        for ( ix = ixmin; ix <= ixmax; ++ix, ++mask) {
            double x = xc + ix * bin;
            *mask = regInsideRegion(region, x, y);
        }
    }
}

double PFX(ComputePixellatedArea)(regRegion * region,
                            long ixmin,
                            long ixmax,
                            long iymin,
                            long iymax,
                            double xc,
                            double yc,
                            double bin )
{
    long npix = 0;
    long iy;
    for ( iy = iymin; iy <= iymax; ++iy ) {
        double y = yc + iy * bin;
        long ix;
        for ( ix = ixmin; ix <= ixmax; ++ix ) {
            double x = xc + ix * bin;
            npix += regInsideRegion(region, x, y);
        }
    }

    return npix * bin * bin;
}


STRLEN
PFX(regRegionToList)(
                     regRegion* region,
                     long ixmin, long ixmax,
                     long iymin, long iymax,
                     double xc, double yc,
                     double bin,
                     SV*  x_sv, SV*  y_sv ) {

    long iy;
    double *xp, *yp, *x0, *xmax;
    STRLEN xlen, ylen, dlen;

    if (SvOK(x_sv) && SvROK (x_sv)) croak("X argument must be a scalar");
    if (SvOK(y_sv) && SvROK (y_sv)) croak("Y argument must be a scalar");

    /* don't really care about the lengths returned from SvPV_force */
    xp = (double*) SvPV_force( x_sv, xlen );
    yp = (double*) SvPV_force( y_sv, ylen );
    xlen = SvLEN( x_sv );
    ylen = SvLEN( y_sv );

    x0 =  xp;
    dlen = ( xlen < ylen ? xlen : ylen ) / sizeof( *xp );
    xmax = xp + dlen;
    for ( iy = iymin; iy <= iymax; ++iy ) {
        double y = yc + iy * bin;
        long ix;
        for ( ix = ixmin; ix <= ixmax; ++ix ) {
            double x = xc + ix * bin;
            if ( regInsideRegion(region, x, y) ) {
                if ( xp == xmax ) {
                    STRLEN offset = xmax - x0;
                    STRLEN len;
                    dlen += 1000;
                    len = dlen * sizeof(*xp);
                    xp = (double*) SvGROW( x_sv, len );
                    yp = (double*) SvGROW( y_sv, len );
                    x0 = xp;
                    xmax = x0 + dlen;
                    xp += offset;
                    yp += offset;
                }
                *xp = x;
                *yp = y;
                ++xp;
                ++yp;
            }
        }
    }

    {
        STRLEN offset = xp - x0;
        STRLEN len = offset * sizeof( *xp );
        SvCUR_set( x_sv, len );
        SvCUR_set( y_sv, len );
        return offset;
    }
}
