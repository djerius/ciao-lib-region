#include <ffi_platypus_bundle.h>
#include <cxcregion.h>
#include <region_priv.h>

#define insideObject( Object, Class, Type )                        \
int regInside##Class ( reg##Object *, double, double ); \
void regInside##Class##_arr_##Type( reg##Object *obj, double* x, double* y, Type* yes, long n) { \
                                                                        \
    for( ; n ; --n, ++x, ++y, ++yes )                                   \
        *yes = regInside##Class ( obj, *x, *y);                         \
}


insideObject( Region, Region, int );
insideObject( Region, Region, short );
insideObject( Region, Region, char );


insideObject( Shape, Shape,int);
insideObject( Shape, Shape,short);
insideObject( Shape, Shape,char);

insideObject( Shape, Annulus, int );
insideObject( Shape, Annulus, short );
insideObject( Shape, Annulus, char );

insideObject( Shape, Box, int );
insideObject( Shape, Box, short );
insideObject( Shape, Box, char );

insideObject( Shape, Circle, int );
insideObject( Shape, Circle, short );
insideObject( Shape, Circle, char );

insideObject( Shape, Ellipse, int );
insideObject( Shape, Ellipse, short );
insideObject( Shape, Ellipse, char );

insideObject( Shape, Field, int );
insideObject( Shape, Field, short );
insideObject( Shape, Field, char );

insideObject( Shape, Pie, int );
insideObject( Shape, Pie, short );
insideObject( Shape, Pie, char );

insideObject( Shape, Point, int );
insideObject( Shape, Point, short );
insideObject( Shape, Point, char );

insideObject( Shape, Polygon, int );
insideObject( Shape, Polygon, short );
insideObject( Shape, Polygon, char );

insideObject( Shape, Rectangle, int );
insideObject( Shape, Rectangle, short );
insideObject( Shape, Rectangle, char );

insideObject( Shape, Sector, int );
insideObject( Shape, Sector, short );
insideObject( Shape, Sector, char );


int reg_get_flag_coord( regShape* shape ) {
    return shape->flag_coord;
}

int reg_get_flag_radius( regShape* shape ) {
    return shape->flag_radius;
}

char* reg_get_flag_name( int );

char* reg_get_flag_coord_name( regShape* shape ) {
    return reg_get_flag_name(shape->flag_coord);
}

char* reg_get_flag_radius_name( regShape* shape ) {
    return reg_get_flag_name(shape->flag_radius);
}

regGeometry reg_get_geometry( regShape* shape ) {
    return shape->type;
}

/* return a copy of the shape so that there's no chance of a memory leak.
   The region will delete the original and Perl will delete the copy
 */
regShape* reg_get_shape_no( const regRegion * region, long shapeNo) {
    return regCopyShape( regGetShapeNo( region, shapeNo ) );
}

/* Add a copy of the shape so that there's no chance of a memory leak.
   The region will delete the original and Perl will delete the copy
 */
int reg_add_shape(  regRegion* region, regMath glue, regShape *shape ) {
    return regAddShape( region, glue, regCopyShape( shape ) );
}
