package CIAO::Lib::Region;

# ABSTRACT: a really awesome library

use strict;
use warnings;

our $VERSION = '0.01';

use Carp ();

use CIAO::Lib::Region::FFI qw( -all );

use POSIX qw( DBL_MAX floor ceil);
use FFI::Platypus;
use FFI::Platypus::Buffer qw( scalar_to_pointer );

use Module::Runtime qw( require_module );
use Ref::Util qw( is_plain_arrayref is_plain_scalarref );
use parent 'Exporter::Tiny';
use Safe::Isa;
use Hash::Wrap;
use Scope::Guard;

use namespace::clean;

use CIAO::Lib::Region::Inside;

use enum qw( :Flavor Exclude Include );
use enum qw( :Math AND OR );
use enum qw( :Coord Unknown Logical Physical World );

use constant Geometry =>
  qw( Point Box Rectangle Circle Ellipse Pie Sector Polygon Annulus Field );
use constant GeometryArr => [Geometry];

use enum ( ':Geo', Geometry );

our %EXPORT_TAGS = (

    constants => [
        qw( FlavorExclude FlavorInclude
          MathAND MathOR
          CoordUnknown CoordLogical CoordPhysical CoordWorld
          )
    ],

    geometry => [ 'Geometry', ( map 'Geo' . $_, Geometry ) ],
);

our @EXPORT_OK = ( map { @$_ } values %EXPORT_TAGS );


# Class constructor; if there's an arg, pass it on to regParse
sub new {
    shift;    # $class
    return @_ ? parse_( shift ) : new_();
}

# library version
{
    my $guard = Scope::Guard::guard { ffi->ignore_not_found(0); };
    ffi->ignore_not_found(1);
    my $func = ffi->function( 'regVersion' => [] => 'string' );
    my $lib_version = ($func // sub{''} )->();
    *lib_version = sub { $lib_version };
}


# Region Create/Free
# regCreateRegion is redundant with regCreateEmptyRegion
ffi->attach( [ regCreateEmptyRegion => 'new_' ] => ['string']   => 'Region_t' );
ffi->attach( [ regCopyRegion        => 'copy' ] => ['Region_t'] => 'Region_t' );
ffi->attach( [ regParse => 'parse_' ]  => ['string']   => 'Region_t' );
ffi->attach( [ regFree  => 'DESTROY' ] => ['Region_t'] => 'void' );

# Region Examination

ffi->attach(
    [ regCompareRegion => 'compare' ] => [ 'Region_t', 'Region_t' ] => 'bool' );
ffi->attach( [ regGetMaxPoints => 'max_points' ] => ['Region_t'] => 'long' );

my @loaded = ( -1 ) x Geometry;
ffi->attach(
    [ reg_get_shape_no => 'shape' ],
    [ 'Region_t', 'long' ],
    'Shape_t',
    sub {
        require CIAO::Lib::Region::Shape;
        my ( $sub, $region, $no ) = @_;
        my $shape    = $sub->( $region, $no );
        my $geometry = $shape->geometry;
        my $class    = __PACKAGE__ . '::' . GeometryArr->[$geometry];
        require_module( $class ) unless ++$loaded[$geometry];
        bless $shape, $class;
    } );

ffi->attach( [ regGetNoShapes => 'num_shapes' ] => ['Region_t'] => 'long' );
ffi->attach(
    [ regOverlapRegion => 'overlap' ] => [ 'Region_t', 'Region_t' ] => 'int' );

# Region Computation

# $region->area( [ $bin, [ \@fieldx, \@fieldy ]  ] );
ffi->attach(
    [ regArea => 'area' ],
    [ 'Region_t', 'double[2]', 'double[2]', 'double' ],
    'double',
    sub {
        my $sub    = shift;
        my $region = shift;
        my $bin    = shift // 1;
        my $fieldx = shift // [ -DBL_MAX(), DBL_MAX() ];
        my $fieldy = shift // [ -DBL_MAX(), DBL_MAX() ];
        return $sub->( $region, $fieldx, $fieldy, $bin );
    } );

# $region->pixel_area( \@xbounds, \@ybounds, $bin );
ffi->attach(
    [ MyPfx . 'ComputePixellatedArea' => 'pixel_area' ],
    #  region,     ixmin,  ixmax,  iymin,  iymax,   xc,       yc,        bin
    [
        'Region_t', 'long', 'long', 'long', 'long', 'double', 'double',
        'double'
    ],
    'double',
    sub {
        my $sub    = shift;
        my $region = shift;

        my %args = @_;

        my ( $xb, $yb )
          = defined $args{bounds}
          ? @{ $args{bounds} }
          : $region->extent;

        my $bin    = $args{bin}    // 1;
        my $center = $args{center} // [ $bin / 2, $bin / 2 ];
        my ( $ixbnd, $iybnd ) = calc_ibnd( $xb, $yb, $center, $bin );
        return $sub->( $region, @$ixbnd, @$iybnd, @$center, $bin );
    } );

# ( \@xpos, \@ypos, $contained  ) = $region->extent( [ \@fieldx, \@fieldy ] );
ffi->attach(
    [ regExtent => 'extent' ],
    [ 'Region_t', 'double[2]', 'double[2]', 'opaque', 'opaque' ],
    'int',
    sub {
        my $sub    = shift;
        my $region = shift;
        my $fieldx = shift // [ -DBL_MAX(), DBL_MAX() ];
        my $fieldy = shift // [ -DBL_MAX(), DBL_MAX() ];
        my ( $xr, $xptr ) = mkbuffer( 2, SIZEOF_DOUBLE );
        my ( $yr, $yptr ) = mkbuffer( 2, SIZEOF_DOUBLE );
        my $contained = $sub->( $region, $fieldx, $fieldy, $xptr, $yptr );
        return [ unpack( 'd2', $$xr ) ], [ unpack( 'd2', $$yr ) ], $contained;
    } );

ffi->attach( [ regGetRegionBounds => 'bounds' ] =>
      [ 'Region_t', 'double[2]', 'double[2]' ] => 'void' );

ffi->attach(
    [ regAppendShape => 'append' ],
    [
        'Region_t', 'string',   'int',  'int',
        'double[]', 'double[]', 'long', 'double[]',
        'double[]', 'int',      'int'
    ],
    sub {
        my $sub = shift;
        my (
            $region,      $shapeName, $includeFlag, $orFlag,
            $xpos,        $ypos,      $radii,       $angles,
            $world_coord, $world_size
        ) = @_;

        $xpos = [$xpos] unless is_plain_arrayref( $xpos );
        $ypos = [$ypos] unless is_plain_arrayref( $ypos );
        # FFI::Platypus (as of 1.10) doesn't convert empty arrays or undef into a NULL pointer.
        # luckily, we can pass in an array of [0] for angles and radii with no ill effect
        $angles
          = is_plain_arrayref( $angles )
          ? @$angles
              ? $angles
              : [0]
          : [ $angles // 0 ];

        $radii
          = is_plain_arrayref( $radii )
          ? @$radii
              ? $radii
              : [0]
          : [ $radii // 0 ];

        my $n = @$xpos;

        Carp::croak( "x and y arrays must have same length\n" )
          if @$ypos != $n;

        $sub->(
            $region, $shapeName,   $includeFlag, $orFlag,
            $xpos,   $ypos,        $n,           $radii,
            $angles,  $world_coord, $world_size
        );

    } );

ffi->attach(
    [ reg_add_shape => 'add' ] => [ 'Region_t', 'int', 'Shape_t' ] => 'int' );

# Region Conversion

#ffi->attach( [ regConvertWorldRegion => '' ] => [ 'Region_t', 'double', 'regInvertFunction invert'] => 'void' );
#ffi->attach( [ regConvertRegion => '' ] => ['regRegion* Region', 'double scale', 'regInvertFunction invert, int force'] => 'void' );

ffi->attach( [ regInvert => 'invert' ] => ['Region_t'], 'Region_t' );


no namespace::clean;

sub calc_imin {
    my ( $min, $c, $bin ) = @_;

    # left edge of bin
    my $edge = $c - $bin / 2;
    $min -= $edge;
    my $imin = floor( $min / $bin );

    return $imin;
}

sub calc_imax {
    my ( $max, $c, $bin ) = @_;

    # right edge of bin
    my $edge = $c + $bin / 2;
    $max -= $edge;
    my $imax = ceil( $max / $bin );

    return $imax;
}

# ( \@ix, \@iy ) = calc_ibnd( \@xb, \@yb, \@c, $bin )
sub calc_ibnd {

    my $bin = pop;
    my $c   = pop;

    return map { [
            calc_imin( $_[$_][0], $c->[0], $bin ),
            calc_imax( $_[$_][1], $c->[1], $bin ) ]
    } 0, 1;
}


sub binned_bounds {
    my ( $self ) = shift;
    my %args = @_;

    my ( $xb, $yb )
      = defined $args{bounds}
      ? @{ $args{bounds} }
      : $self->extent;

    my $bin    = $args{bin}    // 1;
    my $center = $args{center} // [ $bin / 2, $bin / 2 ];
    my ( $ixbnd, $iybnd ) = calc_ibnd( $xb, $yb, $center, $bin );

    my $xlen = $ixbnd->[1] - $ixbnd->[0] + 1;
    my $ylen = $iybnd->[1] - $iybnd->[0] + 1;

    return (
        bin    => $bin,
        center => $center,
        ixbnd  => $ixbnd,
        iybnd  => $iybnd,
        xlen   => $xlen,
        ylen   => $ylen
    );
}

use enum qw( PIDDLE ARRAY SCALAR );

use namespace::clean;

# ($x, $y) = $region->to_list( ?(bounds => [ [ $xmin, $xmax], [ $ymin, $ymax ] ]), ? (bin => $bin ), ?(x => $x), ?(y => $y) )
ffi->attach(
    [ MyPfx . 'regRegionToList' => 'to_list' ],
#  region,     ixmin,  ixmax,  iymin,  iymax,   xc,       yc,        bin,      x (SV),  y(SV)
    [
        'Region_t', 'long',   'long', 'long', 'long', 'double',
        'double',   'double', 'sv',   'sv'
    ],
    'int',
    sub {
        my $sub    = shift;
        my $region = shift;
        my %args   = @_;
        my %bnd    = binned_bounds( $region, %args );

        $args{x} //= [];
        $args{y} //= [];

        my %sv;
        for my $pos ( 'x', 'y' ) {
            if ( is_plain_arrayref( $args{$pos} ) ) {
                my $tmp = '';
                $sv{$pos} = {
                    type => ARRAY,
                    sv   => \$tmp
                };
            }
            elsif ( $args{$pos}->$_isa( 'PDL' ) ) {
                require PDL::Lite;
                my $piddle = $args{$pos};
                $piddle->set_datatype( PDL::double()->enum )
                  if $piddle->type != PDL::short();
                $sv{$pos} = {
                    type => PIDDLE,
                    sv   => $piddle->get_dataref,
                };
            }
            elsif ( is_plain_scalarref $args{$pos} ) {
                ${ $args{$pos} } //= '';
                $sv{$pos} = {
                    type => SCALAR,
                    sv   => $args{$pos},
                };
            }
            else {
                Carp::croak( "unrecognized value passed as $pos\n" );
            }
        }

        my $n = $sub->(
            $region,
            @{ $bnd{ixbnd} },
            @{ $bnd{iybnd} },
            @{ $bnd{center} },
            $bnd{bin}, $sv{x}{sv}, $sv{y}{sv} );

        my @retval;

        return (
            map {
                if ( $sv{$_}{type} == ARRAY ) {
                    [ unpack( 'd*', ${ $sv{$_}{sv} } ) ];
                }
                elsif ( $sv{$_}{type} == SCALAR ) {
                    $sv{$_}{sv};
                }
                elsif ( $sv{$_}{type} == PIDDLE ) {
                    $args{$_}->upd_data;
                    $args{$_}->setdims( [$n] );
                    $args{$_};
                }
                else {
                    Carp::croak( "internal error" );
                }
            } 'x',
            'y'
        );
    },
);

# regRegionToMask => replaced by our own regRegionToMask

# $mask = $region->mask( ?(bounds => [ [ $xmin, $xmax], [ $ymin, $ymax ] ]), ? (bin => $bin ), ?(mask => $mask) )
ffi->attach(
    [ MyPfx . 'RegionToMask' => 'mask' ],
#  region,     ixmin,  ixmax,  iymin,  iymax,   xc,       yc,        bin,   mask
    [
        'Region_t', 'long',   'long', 'long', 'long', 'double',
        'double',   'double', 'opaque'
    ],
    sub {
        my $sub    = shift;
        my $region = shift;
        my %args   = @_;

        my %bnd = binned_bounds( $region, %args );

        my $npix   = $bnd{xlen} * $bnd{ylen};
        my $length = $npix * SIZEOF_SHORT;

        $args{mask} //= [];
        my %mask;
        if ( is_plain_arrayref $args{mask} ) {
            $mask{type} = ARRAY;
            @mask{ 'ref', 'pointer' } = mkbuffer( $length );
        }

        elsif ( $args{mask}->$_isa( 'PDL' ) ) {
            $mask{type} = PIDDLE;
            require PDL::Lite;
            my $mask = $args{mask};
            $mask->set_datatype( PDL::short()->enum )
              if $mask->type != PDL::short();
            $mask->setdims( [ $bnd{xlen}, $bnd{ylen} ] );
            %mask = (
                type    => PIDDLE,
                pointer => scalar_to_pointer( ${ $mask->get_dataref } ),
            );
        }

        elsif ( is_plain_scalarref $args{mask} ) {
            grow( ${ $args{mask} }, $length );
            %mask = (
                ref     => $args{mask},
                type    => SCALAR,
                pointer => scalar_to_pointer( ${ $args{mask} } ) );
        }
        else {
            Carp::croak( "unrecognized value passed as mask\n" );
        }

        $sub->(
            $region,
            @{ $bnd{ixbnd} },
            @{ $bnd{iybnd} },
            @{ $bnd{center} },
            $bnd{bin}, $mask{pointer} );

        if ( $mask{type} == SCALAR ) {

            return wrap_hash {
                ref  => $mask{ref},
                xlen => $bnd{xlen},
                ylen => $bnd{ylen} };
        }

        if ( $mask{type} == ARRAY ) {
            my $skip = -$bnd{xlen};
            @{ $args{mask} } = (
                map {
                    $skip += $bnd{xlen};
                    [
                        unpack(
                            '(x[s])' . $skip . 's' . $bnd{xlen},
                            ${ $mask{ref} } ) ];
                } 1 .. $bnd{ylen} );
        }

        return $args{mask};
    } );



ffi->attach( [ regResolveField => 'resolve_field' ] =>
      [ 'Region_t', 'double[2]', 'double[2]' ] => 'void' );


# Region Logical Combination

ffi->attach( [ regCombineRegion => 'combine' ] => [ 'Region_t', 'Region_t' ] =>
      'Region_t' );
ffi->attach(
    [ regUnionRegion => 'union' ] => [ 'Region_t', 'Region_t' ] => 'Region_t' );
ffi->attach(
    [ regIntersectRegion => 'intersect' ] => [ 'Region_t', 'Region_t' ] =>
      'Region_t' );

# Region Ascii Parsing

ffi->attach( [ regReadAsciiRegion => 'read_ascii' ] => [ 'string', 'int' ] =>
      'Region_t' );
# don't support names
ffi->attach(
    [ regWriteAsciiRegion => 'write_ascii_' ],
    [ 'string', 'Region_t', 'opaque', 'long' ],
    'int',
    sub {
        my ( $sub, $region, $filename ) = @_;
        $sub->( $filename, $region, undef, 0 );
    } );

# Region printing

ffi->attach( [ regPrintRegion => 'print' ] => ['Region_t'] => 'void' );
ffi->attach(
    [ regToStringRegion => 'to_string' ] => ['Region_t'] => 'heap_string' );

# regComposeRegion => not needed; use to_string
# regAllocComposeRegion => deprecated


1;

# COPYRIGHT

__END__


=head1 SYNOPSIS


=head1 SEE ALSO

