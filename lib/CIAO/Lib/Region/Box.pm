package CIAO::Lib::Region::Box;

# ABSTRACT: FFI support for Box shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreateBox => 'new' ],
    # include,   xpos,     ypos,     radius[2],   angle,   wcoord,  wsize
    [ 'enum', 'double*', 'double*', 'double[2]', 'double*', 'int', 'int' ],
    'Box_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $radius, $angle, $wcoord, $wsize ) = @_;
        return $sub->( $include, \$xpos, \$ypos, $radius, \$angle, $wcoord,
            $wsize );
    } );

sub sides;
*sides = \&CIAO::Lib::Region::Shape::radii;

1;
