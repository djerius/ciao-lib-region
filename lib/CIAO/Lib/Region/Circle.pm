package CIAO::Lib::Region::Circle;

# ABSTRACT: FFI support for Circle shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreateCircle => 'new' ],
    # include,   xpos,     ypos,     radius,   wcoord,  wsize
    [ 'enum', 'double*', 'double*', 'double*', 'int', 'int' ],
    'Circle_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $radius, $wcoord, $wsize ) = @_;
        return $sub->( $include, \$xpos, \$ypos, \$radius, $wcoord, $wsize );
    } );
1;
