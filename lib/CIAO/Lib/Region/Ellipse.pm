package CIAO::Lib::Region::Ellipse;

# ABSTRACT: FFI support for Ellipse shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreateEllipse => 'new' ],
    # include,   xpos,     ypos,     radius,      angle    wcoord,  wsize
    [ 'enum', 'double*', 'double*', 'double[2]', 'double*', 'int', 'int' ],
    'Ellipse_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $radius, $angle, $wcoord, $wsize ) = @_;
        return $sub->( $include, \$xpos, \$ypos, $radius, \$angle, $wcoord,
            $wsize );
    } );

1;
