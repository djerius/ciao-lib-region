package CIAO::Lib::Region::FFI;

# ABSTRACT: Container for FFI related things

use strict;
use warnings;

our $VERSION = '0.01';

use Carp ();

use Scalar::Util();
use Ref::Util ();

use base 'DynaLoader';
__PACKAGE__->bootstrap( $VERSION );


use FFI::Platypus;
use FFI::Platypus::Buffer 'scalar_to_pointer';
use FFI::Platypus::Memory 'free';

use Alien::LibCIAORegion;
use parent 'Exporter::Tiny';

use constant ffi => FFI::Platypus->new(
    api => 1,
    lib => [ Alien::LibCIAORegion->dynamic_libs, undef ],
);

use constant ( {
        map { "SIZEOF_" . uc( $_ ) => ffi->sizeof( $_ ) }
          qw[ double int char long short ]
    } );

use constant MyPfx => 'p5_CIAO_LIB_REGION_';

our %EXPORT_TAGS = (

    sizeof =>
      [qw( SIZEOF_DOUBLE SIZEOF_INT SIZEOF_CHAR SIZEOF_LONG SIZEOF_SHORT )],

    utils => [qw( mkbuffer grow update_string )],

    constants => [qw( MyPfx )],
);

our @EXPORT_OK = ( 'ffi', map { @$_ } values %EXPORT_TAGS );

ffi->bundle( 'CIAO::Lib::Region' );

ffi->type( 'object(CIAO::Lib::Region)'        => 'Region_t' );
ffi->type( 'object(CIAO::Lib::Region::Shape)' => 'Shape_t' );

ffi->attach( strlen => ['string'] => 'size_t' );

ffi->custom_type(
    'heap_string' => {
        native_type    => 'opaque',
        native_to_perl => sub {
            my ( $ptr ) = @_;
            my $str
              = ffi->cast( 'opaque' => 'string', $ptr );    # copies the string
            free( $ptr );
            $str;
        }
    } );


ffi->custom_type(
    'sv' => {
        native_type    => 'opaque',
        perl_to_native => sub {
            Carp::croak( "SV must be a reference " )
              unless Ref::Util::is_ref( $_[0] );
            Scalar::Util::refaddr( $_[0] );
        }
    } );

sub update_string {
    set_used_length( $_[0], strlen( $_[0] ) );
}

sub mkbuffer {
    my $length = 1;
    $length *= shift while @_;
    my $buffer;
    grow( $buffer, $length );
    my $pointer = scalar_to_pointer( $buffer );
    return \$buffer, $pointer;
}

1;
