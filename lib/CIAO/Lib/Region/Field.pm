package CIAO::Lib::Region::Field;

# ABSTRACT: FFI support for Field shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreateField => 'new' ],
    # include,   wcoord,  wsize
    [ 'enum', 'int', 'int' ],
    'Field_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $wcoord, $wsize ) = @_;
        return $sub->( $include, $wcoord, $wsize );
    } );

1;
