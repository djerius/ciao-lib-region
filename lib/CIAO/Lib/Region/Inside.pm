package CIAO::Lib::Region::Inside;

use strict;
use warnings;

our $VERSION = '0.01';

use FFI::Platypus::Buffer qw( scalar_to_pointer );
use CIAO::Lib::Region::FFI qw( ffi SIZEOF_CHAR mkbuffer );

use Safe::Isa;
use Package::Stash;
use Ref::Util qw( is_plain_arrayref is_ref );

use namespace::clean;

sub import {

    my $caller = caller;

    my $shape = ( split( '::', $caller ) )[-1];
    my $class = $shape . '_t';

    ffi->attach( [ "regInside${shape}" => "${caller}::inside_" ],
        [ $class, 'double', 'double' ], 'int' );
    ffi->attach( [ "regInside${shape}_arr_int" => "${caller}::inside_arr_int" ],
        [ $class, 'opaque', 'opaque', 'opaque', 'long' ] );
    ffi->attach(
        [ "regInside${shape}_arr_short" => "${caller}::inside_arr_short" ],
        [ $class, 'opaque', 'opaque', 'opaque', 'long' ] );
    ffi->attach(
        [ "regInside${shape}_arr_char" => "${caller}::inside_arr_byte" ],
        [ $class, 'opaque', 'opaque', 'opaque', 'long' ] );

    ffi->attach(
        [ "regInside${shape}_arr_char" => "${caller}::inside_arr_perl" ],
        [ $class, 'double[]', 'double[]', 'opaque', 'long' ],
        sub {
            my $sub = shift;
            my $n   = @{ $_[1] };
            Carp::croak( "position arrays must have the same length\n" )
              if @{ $_[2] } != $n;

            my ( $fr, $fptr ) = mkbuffer( $n, SIZEOF_CHAR );
            $sub->( @_, $fptr, $n );
            return [ unpack( 'c*', $$fr ) ];
        } );

    Package::Stash->new( $caller )->add_symbol( '&inside', \&inside );
}

sub inside {

    my $self = shift;

    if ( !is_ref( $_[0] ) && !is_ref( $_[1] ) ) {
        return $self->inside_( @_ );
    }

    elsif ( is_plain_arrayref( $_[0] ) && is_plain_arrayref( $_[1] ) ) {
        return $self->inside_arr_perl( @_ );
    }

    elsif ( $_[0]->$_isa( 'PDL' ) && $_[1]->$_isa( 'PDL' ) ) {

        require PDL::Lite;
        my ( $x, $y )
          = map { $_->type == PDL::double() ? $_ : $_->double } $_[0],
          $_[1];

        Carp::croak( "position piddles must be one dimensional\n" )
          if $x->ndims != 1;

        Carp::croak( "position piddles must have the same shape\n" )
          if PDL::any( $x->shape != $y->shape );

        my $yes = $_[2]
          // PDL->new_from_specification( PDL::byte(), $x->nelem );
        $yes->setdims( [ $x->nelem ] );

        my $inside = $self->can( 'inside_arr_' . $yes->type->ppforcetype )
          // Carp::croak( "unsupported output piddle type: ", $yes->type,
            "\n" );

        $self->$inside(
            scalar_to_pointer( ${ $x->get_dataref } ),
            scalar_to_pointer( ${ $y->get_dataref } ),
            scalar_to_pointer( ${ $yes->get_dataref } ),
            $x->nelem
        );
        return $yes;
    }

    else {
        Carp::croak( "unrecognizeable position arguments to inside\n" );
    }
}

1;
