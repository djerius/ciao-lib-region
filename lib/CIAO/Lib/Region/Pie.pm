package CIAO::Lib::Region::Pie;

# ABSTRACT: FFI support for Pie shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreatePie => 'new' ],
    # include,   xpos,     ypos,     radius,      angle,     wcoord,  wsize
    [ 'enum', 'double*', 'double*', 'double[2]', 'double[2]', 'int', 'int' ],
    'Pie_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $radius, $angle, $wcoord, $wsize ) = @_;
        return $sub->( $include, \$xpos, \$ypos, $radius, $angle, $wcoord,
            $wsize );
    } );

1;
