package CIAO::Lib::Region::Point;

# ABSTRACT: FFI support for Point shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreatePoint => 'new' ],
    # include,   xpos,     ypos,   wcoord,  wsize
    [ 'enum', 'double*', 'double*', 'int', 'int' ],
    'Point_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $wcoord, $wsize ) = @_;
        return $sub->( $include, \$xpos, \$ypos, $wcoord, $wsize );
    } );

1;
