package CIAO::Lib::Region::Polygon;

# ABSTRACT: FFI support for Polygon shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreatePolygon => 'new' ],
    # include,   xpos,     ypos,     npoints, wcoord,  wsize
    [ 'enum', 'double[]', 'double[]', 'long', 'int', 'int' ],
    'Polygon_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $wcoord, $wsize ) = @_;
        my $npos = @$xpos;
        die( "xpos and ypos must have same number of elements\n" )
          unless $npos == @$ypos;
        return $sub->( $include, $xpos, $ypos, $npos, $wcoord, $wsize );
    } );

1;
