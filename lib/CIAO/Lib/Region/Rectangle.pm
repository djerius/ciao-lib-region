package CIAO::Lib::Region::Rectangle;

# ABSTRACT: FFI support for Rectangle shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreateRectangle => 'new' ],
    # include,   xpos,        ypos,     angle,   wcoord,  wsize
    [ 'enum', 'double[2]', 'double[2]', 'double*', 'int', 'int' ],
    'Rectangle_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $angle, $wcoord, $wsize ) = @_;
        return $sub->( $include, $xpos, $ypos, \$angle, $wcoord, $wsize );
    } );

1;
