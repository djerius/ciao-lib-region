package CIAO::Lib::Region::Sector;

# ABSTRACT: FFI support for Sector shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use CIAO::Lib::Region::FFI 'ffi';

use namespace::clean;

use CIAO::Lib::Region::Shape::Common;

ffi->attach(
    [ regCreateSector => 'new' ],
    # include,   xpos,     ypos,     angle,       wcoord,  wsize
    [ 'enum', 'double*', 'double*', 'double[2]', 'int', 'int' ],
    'Sector_t',
    sub {
        my $sub   = shift;
        my $class = shift;
        my ( $include, $xpos, $ypos, $angle, $wcoord, $wsize ) = @_;
        return $sub->( $include, \$xpos, \$ypos, $angle, $wcoord, $wsize );
    } );

1;
