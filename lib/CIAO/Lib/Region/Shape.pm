package CIAO::Lib::Region::Shape;

# ABSTRACT: FFI support for Generic Shapes

use strict;
use warnings;
our $VERSION = '0.01';

use Safe::Isa;
use FFI::Platypus::Buffer qw( scalar_to_pointer );
use CIAO::Lib::Region::FFI -all;
use Ref::Util qw( is_plain_arrayref is_ref );

use namespace::clean;

use CIAO::Lib::Region::Inside;

# Shape Accessors

ffi->attach(
    [ regShapeGetName => 'name' ] => [ 'Shape_t', 'string', 'long' ] =>
      'int' => sub {
        my ( $sub, $shape ) = @_;
        my $len = 128
          ;   # The longest shape name is "RotRectangle", but just to be safe...
        my $buf;
        grow( $buf, $len + 1 );
        $sub->( $shape, $buf, $len );
        update_string( $buf );
        return $buf;
    } );

ffi->attach(
    [ regShapeGetPoints => 'points' ] =>
      [ 'Shape_t', 'opaque', 'opaque', 'long' ] => 'long',
    sub {
        my ( $sub, $shape ) = @_;
        my $npts = $shape->num_points;

        # can't just assign $xp to $yp, as direct C manipulation of the buffer
        # avoids Perl's Copy-On-Write optimization.
        my ( $xr, $xptr ) = mkbuffer( $npts, SIZEOF_DOUBLE );
        my ( $yr, $yptr ) = mkbuffer( $npts, SIZEOF_DOUBLE );
        my $got = $sub->( $shape, $xptr, $yptr, $npts );
        return map { [ unpack( 'd*', $_ ) ] } $$xr, $$yr;
    } );

ffi->attach(
    [ regShapeGetPoints => 'point' ],
    [ 'Shape_t', 'double*', 'double*', 'long' ],
    'long',
    sub {
        my ( $sub, $shape ) = @_;
        my ( $x, $y );
        my $got = $sub->( $shape, \$x, \$y, 1 );
        return ( $x, $y );
    } );

ffi->attach( [ reg_shape_angles => 'num_angles' ] => ['Shape_t'] => 'long' );
ffi->attach(
    [ regShapeGetAngles => 'angles' ] => [ 'Shape_t', 'opaque' ] => 'long' =>
      sub {
        my ( $sub, $shape ) = @_;
        my $nangles = $shape->num_angles;
        my ( $ar, $aptr ) = mkbuffer( $nangles, SIZEOF_DOUBLE );
        my $got = $sub->( $shape, $aptr, $nangles );
        return [ unpack( 'd*', $$ar ) ];
    } );

ffi->attach(
    [ regShapeGetAngles => 'angle' ] => [ 'Shape_t', 'double*' ] => 'long' =>
      sub {
        my ( $sub, $shape ) = @_;
        my $angle;
        my $got = $sub->( $shape, \$angle, 1 );
        return $angle;
    } );

ffi->attach( [ reg_shape_radii => 'num_radii' ] => ['Shape_t'] => 'long' );
ffi->attach(
    [ regShapeGetRadii => 'radii' ] => [ 'Shape_t', 'opaque' ] => 'long' =>
      sub {
        my ( $sub, $shape ) = @_;
        my $nradii = $shape->num_radii;
        my ( $rr, $rptr ) = mkbuffer( $nradii, SIZEOF_DOUBLE );
        my $got = $sub->( $shape, $rptr, $nradii );
        return [ unpack( 'd*', $$rr ) ];
    } );

ffi->attach(
    [ regShapeGetRadii => 'radius' ] => [ 'Shape_t', 'double*' ] => 'long' =>
      sub {
        my ( $sub, $shape ) = @_;
        my $radius;
        my $got = $sub->( $shape, \$radius, 1 );
        return $radius;
    } );

ffi->attach( [ regShapeGetNoPoints => 'num_points' ] => ['Shape_t'] => 'long' );
ffi->attach( [ regShapeGetComponent => 'component' ] => ['Shape_t'] => 'long' );
ffi->attach( [ reg_get_flag_coord  => 'coord_flag' ]  => ['Shape_t'] => 'int' );
ffi->attach( [ reg_get_flag_radius => 'radius_flag' ] => ['Shape_t'] => 'int' );
ffi->attach(
    [ reg_get_flag_coord_name => 'coord_name' ] => ['Shape_t'] => 'string' );
ffi->attach(
    [ reg_get_flag_radius_name => 'radius_name' ] => ['Shape_t'] => 'string' );
ffi->attach( [ reg_get_geometry => 'geometry' ] => ['Shape_t'] => 'int' );

# Shape Operations
ffi->attach(
    [ regCompareShape => 'compare' ] => [ 'Shape_t', 'Shape_t', 'short' ] =>
      'int' );
ffi->attach( [ regCopyShape  => 'copy' ]  => ['Shape_t'] => 'Shape_t' );
ffi->attach( [ regPrintShape => 'print' ] => ['Shape_t'] => 'void' );

1;
