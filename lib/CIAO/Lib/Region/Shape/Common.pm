package CIAO::Lib::Region::Shape::Common;

use strict;
use warnings;

our $VERSION = '0.01';

use FFI::Platypus::Buffer qw( scalar_to_pointer );
use CIAO::Lib::Region::FFI -all;
use Import::Into;

# ABSTRACT: Common code generation for shapes

use namespace::clean;


sub import {

    my $caller = caller;

    my $shape = ( split( '::', $caller ) )[-1];
    my $class = $shape . '_t';

    ffi->type( "object($caller)" => $class );
    ffi->attach( [ "regCopy${shape}" => "${caller}::copy" ],
        [$class] => $class );
    ffi->attach( [ "regCalcArea${shape}" => "${caller}::area" ],
        [$class] => 'double' );

    # ( $\@xpos, \@ypos ) = $shape->extent;
    ffi->attach(
        [ "regCalcExtent${shape}" => "${caller}::extent" ],
        [ $class, 'double[2]', 'double[2]' ],
        sub {
            my $sub   = shift;
            my $shape = shift;
            my @x     = ( 0, 0 );
            my @y     = ( 0, 0 );
            $sub->( $shape, \@x, \@y );
            return ( \@x, \@y );
        } );

    ffi->attach(
        [ "regToString${shape}" => "${caller}::to_string" ],
        [ $class, 'string', 'long' ],
        sub {
            my ( $sub, $shape ) = @_;
            my $num_points = $shape->num_points;
            my $len = $num_points > 2 ? ( 80 + ( 20 * $num_points ) ) : 120;
            my $buf;
            grow( $buf, $len );
            $sub->( $shape, $buf, $len );
            update_string( $buf );
            $buf;
        } );

    CIAO::Lib::Region::Inside->import::into( $caller );
}

1;
