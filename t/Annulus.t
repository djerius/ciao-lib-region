#! perl
use Test2::V0;

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Annulus';

use Math::Trig qw[ pi ];

my $shape;

ok(
    lives {
        $shape = Annulus->new( FlavorInclude, 10, 8, [ 11, 33 ],
            CoordPhysical, CoordPhysical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [ 10, 8 ], "correct position" );

my $radii;
ok( lives { $radii = $shape->radii }, "get radii" );
is( $radii, [ 11, 33 ], "correct radii" );

is( $shape->area, pi * ( 33**2 - 11**2 ), "area" );
is( [ $shape->extent ], [ [ 10 - 33, 10 + 33 ], [ 8 - 33, 8 + 33 ] ],
    "extent" );

is( $shape->to_string, "Annulus(10,8,11,33)", "string" );

is( $shape->name, "Annulus", "name" );
done_testing;
