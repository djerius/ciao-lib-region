#! perl
use Test2::V0;
use Test::Lib;

use MyTest::Common;

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Box';

use List::Util qw( max min );

use constant {
    XCEN  => 11,
    YCEN  => 14,
    XLEN  => 22,
    YLEN  => 44,
    THETA => 33,
};

my $shape;

ok(
    lives {
        $shape = Box->new( FlavorInclude, XCEN, YCEN, [ XLEN, YLEN ],
            THETA, CoordPhysical, CoordPhysical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [ XCEN, YCEN ], "correct position" );

my $sides;
ok( lives { $sides = $shape->sides }, "get sides" );
is( $sides, [ XLEN, YLEN ], "correct sides" );

my $angle;
ok( lives { $angle = $shape->angle }, "get angle" );
is( $angle, THETA, "correct angle" );

is( $shape->area, XLEN * YLEN, 'area' );


{
    my %bnd = bound_rotated_box( XLEN, YLEN, THETA );

    is(
        [ $shape->extent ],
        [
            [ XCEN + $bnd{xmin}, XCEN + $bnd{xmax} ],
            [ YCEN + $bnd{ymin}, YCEN + $bnd{ymax} ],
        ],
        'extent'
    );
}

is(
    $shape->to_string,
    sprintf(
        "RotBox(%.0f,%.0f,%.0f,%.0f,%.0f)", XCEN, YCEN, XLEN, YLEN, THETA
    ),
    "string"
);
is( $shape->name, 'RotBox', 'name' );

done_testing;
