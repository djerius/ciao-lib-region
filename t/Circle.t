#! perl
use Test2::V0;
use Test::Lib;

use MyTest::Common;

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Circle';

use Math::Trig qw( pi );

my $shape;

ok(
    lives {
        $shape = Circle->new( FlavorInclude, 10, 8, 33, CoordPhysical,
            CoordPhysical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [ 10, 8 ], "correct position" );

my $radius;
ok( lives { $radius = $shape->radius }, "get radius" );
is( $radius, 33, "correct radius" );

is( $shape->area, pi * $radius**2, "area" );

is( $shape->to_string, "Circle(10,8,33)", "string" );

is( [ $shape->extent ], [ [ 10 - 33, 10 + 33 ], [ 8 - 33, 8 + 33 ] ],
    'extent' );
is( $shape->name, 'Circle', 'name' );



done_testing;
