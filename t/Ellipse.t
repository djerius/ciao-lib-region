#! perl
use Test2::V0;
use Test::Lib;

use MyTest::Common;

use Math::Trig qw( pi deg2rad );

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Ellipse';

use constant {
    XCEN       => 0,
    YCEN       => 0,
    SEMI_MAJOR => 44,
    SEMI_MINOR => 33,
    THETA      => 55
};

my $shape;

ok(
    lives {
        $shape
          = Ellipse->new( FlavorInclude, XCEN, YCEN, [ SEMI_MAJOR, SEMI_MINOR ],
            THETA, CoordPhysical, CoordPhysical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [ XCEN, YCEN ], "correct position" );

my $radii;
ok( lives { $radii = $shape->radii }, "get radii" );
is( $radii, [ SEMI_MAJOR, SEMI_MINOR ], "correct radii" );

my $angle;
ok( lives { $angle = $shape->angle }, "get angle" );
is( $angle, THETA, "correct angle" );

is( $shape->area, pi * SEMI_MAJOR * SEMI_MINOR, "area" );

if ( CIAO::Lib::Region->lib_version ) {

    # see https://math.stackexchange.com/a/91304
    # x = ± sqrt( a^2 * cos(θ)^2 + b^2 * sin(θ)^2 )
    # y = ± sqrt( a^2 * sin(θ)^2 + b^2 * cos(θ)^2 )

    my $theta = deg2rad( THETA );

    my $xbnd = sqrt(
        ( SEMI_MAJOR * cos( $theta ) )**2 + ( SEMI_MINOR * sin( $theta ) )**2 );
    my $ybnd = sqrt(
        ( SEMI_MAJOR * sin( $theta ) )**2 + ( SEMI_MINOR * cos( $theta ) )**2 );

    is(
        [ $shape->extent ],
        [ [ XCEN - $xbnd, XCEN + $xbnd ], [ YCEN - $ybnd, YCEN + $ybnd ] ],
        "extent using optimal bounding box"
    );
}

else {

    my %bnd = bound_rotated_box( 2 * SEMI_MAJOR, 2 * SEMI_MINOR, THETA );

    is(
        [ $shape->extent ],
        [
            [ XCEN + $bnd{xmin}, XCEN + $bnd{xmax} ],
            [ YCEN + $bnd{ymin}, YCEN + $bnd{ymax} ]
        ],
        "extent using rotated bounding box"
    );
}

is(
    $shape->to_string,
    sprintf(
        "Ellipse(%.0f,%.0f,%.0f,%.0f,%.0f)",
        XCEN, YCEN, SEMI_MAJOR, SEMI_MINOR, THETA
    ),
    "string"
);
is( $shape->name, "Ellipse", 'name' );

done_testing;
