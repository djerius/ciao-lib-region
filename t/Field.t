#! perl
use Test2::V0;

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Field';

my $shape;

ok(
    lives {
        $shape = Field->new( FlavorInclude, CoordPhysical, CoordPhysical )
    },
    "construct object"
) or note $@;

is( $shape->area, 0, "area" );
is( [ $shape->extent ], [ [ 0, 0 ], [ 0, 0 ] ], "extent" );

is( $shape->to_string, "Field()", "string" );

is( $shape->name, "Field", "name" );

done_testing;
