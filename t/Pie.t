#! perl
use Test2::V0;

use Math::Trig qw( pi deg2rad tan );

use CIAO::Lib::Region - constants;
use aliased 'CIAO::Lib::Region::Pie';

my $shape;

ok(
    lives {
        $shape = Pie->new( FlavorInclude, 10, 8,
            [ 11, 33 ],    # radius
            [ 44, 55 ],    # angle
            CoordPhysical, CoordPhysical
        )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [ 10, 8 ], "correct position" );

my $radii;
ok( lives { $radii = $shape->radii }, "get radii" );
is( $radii, [ 11, 33 ], "correct radii" );

my $angles;
ok( lives { $angles = $shape->angles }, "get angles" );
is( $angles, [ 44, 55 ], "correct angles" );

is( $shape->area, ( 55 - 44 ) / 360 * pi * ( 33**2 - 11**2 ), 'area' );

{
    my $xmin = 10 + 11 * cos( deg2rad( 55 ) );
    my $xmax = 10 + 33 * cos( deg2rad( 44 ) );

    my $ymin = 8 + 11 * sin( deg2rad( 44 ) );
    my $ymax = 8 + 33 * sin( deg2rad( 55 ) );

    is( [ $shape->extent ], [ [ $xmin, $xmax ], [ $ymin, $ymax ] ], "extent" );
}



is( $shape->to_string, "Pie(10,8,11,33,44,55)", "string" );
is( $shape->name, "Pie", "name" );

done_testing;
