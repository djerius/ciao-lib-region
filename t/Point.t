#! perl
use Test2::V0;

use CIAO::Lib::Region - constants;
use aliased 'CIAO::Lib::Region::Point';

my $shape;

ok(
    lives {
        $shape
          = Point->new( FlavorInclude, 10, 8, CoordPhysical, CoordPhysical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "retrieve points" );

is( [ $x, $y ], [ 10, 8 ], "correct position" );

is( $shape->area, 0, "area" );

is( [ $shape->extent ], [ [ 10, 10 ], [ 8, 8 ] ], "extent" );

is( $shape->to_string, "Point(10,8)", "string" );

is( $shape->name, "Point", "name" );

done_testing;
