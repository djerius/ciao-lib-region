#! perl
use Test2::V0;

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Polygon';

my $shape;

ok(
    lives {
        $shape = Polygon->new( FlavorInclude, [ 10, 11, 12, 13 ],
            [ 14, 15, 16, 17 ], CoordPhysical,
            CoordPhysical
        )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->points }, "get points" );
is( $x, [ 10, 11, 12, 13, 10 ], "correct x" );
is( $y, [ 14, 15, 16, 17, 14 ], "correct y" );

is( $shape->area, 0, "area" );
is( [ $shape->extent ], [ [ 10, 13 ], [ 14, 17 ] ], "extent" );

is( $shape->to_string, "Polygon(10,14,11,15,12,16,13,17,10,14)", "string" );

is( $shape->name, 'Polygon', 'name' );

done_testing;
