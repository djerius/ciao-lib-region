#! perl
use Test2::V0;

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Rectangle';

my $shape;

ok(
    lives {
        $shape = Rectangle->new( FlavorInclude,
            [ 8,  10 ],
            [ 11, 33 ],
            44, CoordPhysical, CoordPhysical
        )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->points }, "get points" );
is( $x, [ 8,  10 ], "x position" );
is( $y, [ 11, 33 ], "y position" );

my $angle;
ok( lives { $angle = $shape->angle }, "get angle" );
is( $angle, 44, "correct angle" );

SKIP: {
    skip(
        "testing against version of cxcregion which returns incorrect area for rotated rectangles"
    ) if !CIAO::Lib::Region->lib_version;

    is( $shape->area, ( 10 - 8 ) * ( 33 - 11 ), "area" );
}

is( [ $shape->extent ], [ [ 8, 10 ], [ 11, 33 ] ], "extent" );

is( $shape->to_string, "RotRectangle(8,11,10,33,44)", "string" );

is( $shape->name, "RotRectangle", 'name' );

done_testing;
