#! perl
use Test2::V0;

use Test::Lib;

use MyTest::Common;
use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region';
use aliased 'CIAO::Lib::Region::Point';
use aliased 'CIAO::Lib::Region::Box';

{
    my $version = Region->lib_version;
    note "Using cxcregion version: ", $version eq '' ? 'unknown' : $version;
}


subtest 'basic test' => sub {
    my $region;

    ok(
        lives {
            $region = Region->new;
        },
        "construct region"
    ) or note $@;

    is( $region->num_shapes, 0, "object is empty" );


    ok(
        lives {
            $region->append( 'point', FlavorInclude, MathAND, 2, 3, [], [],
                CoordPhysical, CoordLogical );
        },
        'append point'
    ) or note $@;

    is( $region->num_shapes, 1, "added an object" );

    ok(
        lives {
            $region->append(
                'box', FlavorInclude, MathAND, 0,
                0, [ 2, 2 ], 10, CoordPhysical,
                CoordLogical
            );
        },
        'append box'
    ) or note $@;


    is( $region->num_shapes, 2, "added another object" );

    my $shape;
    ok( lives { $shape = $region->shape( 1 ) }, 'extract shape #1' ) or note $@;
    isa_ok( $shape, ['CIAO::Lib::Region::Point'], 'shape is in Point class' );
    ok( lives { $shape = $region->shape( 2 ) }, 'extract shape #2' ) or note $@;
    isa_ok( $shape, ['CIAO::Lib::Region::Box'], 'shape is in Box class' );

    is( $region->to_string, "Point(2,3)&RotBox(0,0,2,2,10)", "string" );

};

subtest 'append & retrieve shapes' => sub {

    my $region = Region->new;

    my $args = sub {
        'point', FlavorInclude, MathAND, [ $_[0] ], [ $_[0] + 1 ], [], [],
          CoordPhysical, CoordLogical;
    };

    $region->append( $args->( $_ ) ) for 1 .. 1000;

    is( $region->num_shapes, 1000, "appended 1000 points" );

    {
        my @ids = ( 1 .. 1000 );

        # retrieve, build, and destroy Perl objects.
        my @got = map { $region->shape( $_ )->component } @ids;

        is( @got, @ids, "retrieved 1000 components" );
    }


    # add a bunch more, maybe reuse memory, then reread
    $region->append( $args->( $_ ) ) for 1001 .. 2000;

    is( $region->num_shapes, 2000, "appended 2000 points" );

    {
        my @ids = ( 1 .. 2000 );

        # retrieve, build, and destroy Perl objects.
        my @got = map { $region->shape( $_ )->component } @ids;

        is( @got, @ids, "retrieved 2000 components" );
    }

};

subtest 'add & retrieve shapes' => sub {

    my $region = Region->new;

    my $shape = sub {
        Point->new( FlavorInclude, MathAND, $_[0], $_[0] + 1,
            CoordPhysical, CoordLogical );
    };

    $region->add( MathAND, $shape->( $_ ) ) for 1 .. 1000;

    is( $region->num_shapes, 1000, "added 1000 points" );

    {
        my @ids = ( 1 .. 1000 );

        # retrieve, build, and destroy Perl objects.
        my @got = map { $region->shape( $_ )->component } @ids;

        is( @got, @ids, "retrieved 1000 components" );
    }


    # add a bunch more, maybe reuse memory, then reread
    $region->add( MathAND, $shape->( $_ ) ) for 1001 .. 2000;

    is( $region->num_shapes, 2000, "appended 2000 points" );

    {
        my @ids = ( 1 .. 2000 );

        # retrieve, build, and destroy Perl objects.
        my @got = map { $region->shape( $_ )->component } @ids;

        is( @got, @ids, "retrieved 2000 components" );
    }

};

subtest 'extent' => sub {

    my $region = Region->new;
    my $shape  = Box->new( FlavorInclude, 0, 0, [ 2, 2 ], 0, CoordPhysical,
        CoordPhysical );

    {
        my ( $x, $y ) = $shape->extent;
        is( $x, [ -1, 1 ], "Box x extent" );
        is( $y, [ -1, 1 ], "Box y extent" );
    }

    $region->add( MathAND, $shape );
    {
        my ( $x, $y, $contained ) = $region->extent;
        is( $contained, 1, "Contained region" );
        is( $x, [ -1, 1 ], "Region x extent" );
        is( $y, [ -1, 1 ], "Region y extent" );
    }

};


subtest 'area' => sub {

    my $region = Region->new;
    my $shape  = Box->new( FlavorInclude, 0, 0, [ 2, 2 ], 0, CoordPhysical,
        CoordPhysical );
    $region->add( MathAND, $shape );

    is( $shape->area,  4, "shape area" );
    is( $region->area, 4, "region area" );

};

subtest 'pixellated area' => sub {

    my $region = Region->new;
    my $shape  = Box->new( FlavorInclude, 0, 0, [ 2, 2 ], 0, CoordPhysical,
        CoordPhysical );
    $region->add( MathAND, $shape );

    is( $region->pixel_area(), 4, "region area" );

};

subtest 'mask' => sub {

    my $region = Region->new;
    my $shape  = Box->new( FlavorInclude, 0, 0, [ 2, 2 ], 0, CoordPhysical,
        CoordPhysical );
    $region->add( MathAND, $shape );

    my $expected = [ [ 1, 1 ], [ 1, 1 ] ];

    subtest 'array' => sub {
        my $mask = [];
        $region->mask( mask => $mask );
        is( $mask, $expected, "mask" );
    };

    subtest 'scalar' => sub {
        my $sclr;

        my $mask = $region->mask( mask => \$sclr );

        my $skip = -$mask->xlen;
        my @mask = (
            map {
                $skip += $mask->xlen;
                [
                    unpack(
                        '(x[s])' . $skip . 's' . $mask->xlen,
                        ${ $mask->ref } ) ];
            } 1 .. $mask->ylen
        );
        is( \@mask, $expected, "mask" );
    };

    subtest 'PDL' => sub {
      SKIP: {
            skip( 'requires PDL' ) unless has_PDL;
            require PDL::Lite;
            my $mask = $region->mask( mask => PDL->new );
            is( $mask->unpdl, $expected, "mask" );
        }
    };

};

subtest 'to_list' => sub {

    my $region = Region->new;
    my $shape  = Box->new( FlavorInclude, 0, 0, [ 2, 2 ], 0, CoordPhysical,
        CoordPhysical );
    $region->add( MathAND, $shape );

    my $ex = [ -0.5, 0.5,  -0.5, 0.5 ];
    my $ey = [ -0.5, -0.5, 0.5,  0.5 ];

    subtest 'array' => sub {
        my ( $x, $y ) = $region->to_list;
        is( $x, $ex, "x" );
        is( $y, $ey, "y" );
    };

    subtest 'scalar' => sub {
        my ( $xb, $yb );

        $region->to_list( x => \$xb, y => \$yb );
        my ( $x, $y ) = map { [ unpack( 'd*', $_ ) ] } $xb, $yb;

        is( $x, $ex, "x" );
        is( $y, $ey, "y" );
    };

    subtest 'PDL' => sub {
      SKIP: {
            skip( 'requires PDL' ) unless has_PDL;
            require PDL::Lite;

            my ( $x, $y ) = $region->to_list( x => PDL->new, y => PDL->new );
            is( $x->unpdl, $ex, "x" );
            is( $y->unpdl, $ey, "y" );
        }
    };

};

done_testing;
