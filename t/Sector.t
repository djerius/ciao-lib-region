#! perl
use Test2::V0;

use CIAO::Lib::Region -constants;
use aliased 'CIAO::Lib::Region::Sector';

my $shape;

ok(
    lives {
        $shape = Sector->new( FlavorInclude, 10, 8, [ 44, 55 ],
            CoordPhysical, CoordPhysical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [ 10, 8 ], "correct position" );

my $angles;
ok( lives { $angles = $shape->angles }, "get angles" );
is( $angles, [ 44, 55 ], "correct angles" );

is( $shape->area, 1, "area" );
is( [ $shape->extent ], [ [ 0, 0 ], [ 0, 0 ] ], "extent" );

is( $shape->to_string, "Sector(10,8,44,55)", "string" );
is( $shape->name,      'Sector',             'name' );

done_testing;
