#! perl
use Test2::V0;
use Test::Lib;

use MyTest::Common;
use Ref::Util qw[ is_coderef ];
use Module::Runtime 'require_module';

use CIAO::Lib::Region -constants, -geometry;

use aliased 'CIAO::Lib::Region';

use Hash::Wrap;

my %Test = (

    Point => {
        pars => {
            xc => 0,
            yc => 0,
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{xc}, $pars{yc}, CoordPhysical,
                CoordPhysical
            );
        },
        x      => [ 0, 4, 3 ],
        y      => [ 0, 4, 3 ],
        inside => [ 1, 0, 0 ],
    },

    Box => {
        pars => {
            xc    => 0,
            yc    => 0,
            sides => [ 4, 2 ],
            angle => 0,
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{xc}, $pars{yc}, $pars{sides},
                $pars{angle}, CoordPhysical, CoordPhysical
            );
        },
        x      => [ 0, 4, 1.5 ],
        y      => [ 0, 4, 0.5 ],
        inside => [ 1, 0, 1 ],
    },

    Rectangle => {
        pars => {
            x     => [ 0, 10 ],
            y     => [ 3, 5 ],
            angle => 0,
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{x}, $pars{y}, $pars{angle},
                CoordPhysical, CoordPhysical
            );
        },
        x      => [ 0, 4, 5 ],
        y      => [ 0, 4, 4 ],
        inside => [ 0, 1, 1 ],
    },

    Circle => {
        pars => {
            xc     => 0,
            yc     => 0,
            radius => 4.5,
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{xc}, $pars{yc}, $pars{radius},
                CoordPhysical, CoordPhysical
            );
        },
        x      => [ 0, 4, 3 ],
        y      => [ 0, 4, 3 ],
        inside => [ 1, 0, 1 ],
    },

    Ellipse => {
        pars => {
            xc     => 0,
            yc     => 0,
            radius => [ 4, 5 ],
            angle  => 0,
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{xc}, $pars{yc}, $pars{radius},
                $pars{angle}, CoordPhysical, CoordPhysical
            );
        },
        x      => [ 0, 4, 3 ],
        y      => [ 0, 4, 3 ],
        inside => [ 1, 0, 1 ],
    },

    Pie => {
        pars => {
            xc     => 0,
            yc     => 0,
            radius => [ 2, 5 ],
            angle  => [ 0, 45 ],
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{xc}, $pars{yc}, $pars{radius},
                $pars{angle}, CoordPhysical, CoordPhysical
            );
        },
        x      => [ 0, 4, 2 ],
        y      => [ 0, 5, 1.5 ],
        inside => [ 0, 0, 1 ],
    },

    Sector => {
        pars => {
            xc    => 0,
            yc    => 0,
            angle => [ 0, 45 ],
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{xc}, $pars{yc}, $pars{angle},
                CoordPhysical, CoordPhysical
            );
        },
        x      => [ 0, 4,  2 ],
        y      => [ 0, 10, 1.5 ],
        inside => [ 1, 0,  1 ],
    },

    Polygon => {
        pars => {
            x => [ 0, 1, 1, 0 ],
            y => [ 0, 0, 1, 1 ]
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{x}, $pars{y}, CoordPhysical, CoordPhysical );
        },
        x      => [ 0, 4,  0.5 ],
        y      => [ 0, 10, 0.5 ],
        inside => [ 1, 0,  1 ],
    },

    Annulus => {
        pars => {
            xc     => 0,
            yc     => 0,
            radius => [ 10, 20 ],
        },
        args => sub {
            my %pars = @_;
            ( FlavorInclude, $pars{xc}, $pars{yc}, $pars{radius},
                CoordPhysical, CoordPhysical
            );
        },
        x      => [ 0, 11, 20 ],
        y      => [ 0, 11, 20 ],
        inside => [ 0, 1,  0 ],
    },

    Field => {
        pars => {},
        args => sub {
            ( FlavorInclude, CoordPhysical, CoordPhysical );
        },
        x      => [ 0, 11, 20 ],
        y      => [ 1, 11, 20 ],
        inside => [ 1, 1,  1 ],
    },
);


for my $geometry ( Geometry() ) {

    subtest $geometry => sub {

        my $test = wrap_hash( $Test{$geometry}
              // bail_out "missing test for $geometry\n" );

        my %pars = %{ $test->pars };
        my $x    = $test->x;
        my $y    = $test->y;
        my $inside
          = is_coderef( $test->inside )
          ? $test->inside->( $x, $y, %pars )
          : $test->inside;

        my $shape;
        subtest 'shape' => sub {

            my $class = "CIAO::Lib::Region::$geometry";
            require_module( $class );
            ok( lives { $shape = $class->new( $test->args->( %pars ) ) },
                "construct object" )
              or die $@;

            test_inside( $shape, $x, $y, $inside );
        };

        subtest 'region' => sub {
            my $region = Region->new;
            $region->add( MathAND, $shape );
            test_inside( $region, $x, $y, $inside );
        };
    };
}


sub test_inside {

    my $ctx = context();

    my ( $object, $x, $y, $inside ) = @_;

    subtest 'scalar' => sub {
        for my $idx ( 0 .. $#{$x} ) {
            is(
                $object->inside( $x->[$idx], $y->[$idx] ),
                $inside->[$idx],
                sprintf(
                    "(%.1f,%.1f) is %s",
                    $x->[$idx], $y->[$idx], $inside->[$idx] ? 'in' : 'out'
                ) );
        }
    };

    subtest 'Perl array' => sub {
        is( $object->inside( $x, $y ), $inside, 'inside' );
    };

    subtest 'PDL' => sub {

      SKIP: {
            skip( "requires PDL" ) unless has_PDL;
            require PDL::Lite;

            subtest "no return piddle" => sub {
                my $yes = $object->inside( PDL->new( $x ), PDL->new( $y ) );
                is( $yes->unpdl, $inside, 'PDL, create return piddle' )
                  or note $yes;
            };

            subtest "null return piddle" => sub {
                my $yes
                  = $object->inside( PDL->new( $x ), PDL->new( $y ),
                    PDL->null );
                is( $yes->unpdl, $inside, 'PDL, create return piddle' )
                  or note $yes;
            };

            for my $type ( PDL::short(), PDL::long(), PDL::byte() ) {
                my $name = $type->shortctype;
                subtest "$name return piddle" => sub {
                    my $yes = $object->inside(
                        PDL->new( $x ),
                        PDL->new( $y ),
                        PDL->new( $type ) );
                    is( $yes->unpdl, $inside, 'got expected result' )
                      or note $yes;
                };
            }

            for my $type ( PDL::double(), PDL::float() ) {
                my $name = $type->shortctype;
                subtest "unsupported $type return piddle" => sub {
                    like(
                        dies {
                            $object->inside(
                                PDL->new( $x ),
                                PDL->new( $y ),
                                PDL->new( $type ) )
                        },
                        qr/unsupported/
                    );
                };
            }
        }
    };

    $ctx->release();
}


done_testing;
