#! perl

use Test2::V0;

use CIAO::Lib::Region::FFI 'grow';
use Scalar::Util 'refaddr';
use B;

my $s = "abdef";

grow( $s, 100 );

ok( B::svref_2object( \$s )->LEN > 100 );

done_testing;
