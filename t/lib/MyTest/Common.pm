package MyTest::Common;

use Math::Trig qw( pi deg2rad );
use List::Util qw( max min );


use parent 'Exporter::Tiny';

use constant has_PDL => !! eval { require PDL::Lite; 1 };

our @EXPORT = qw( has_PDL rotate bound_rotated_box );

sub rotate {
    my ( $x, $y, $theta ) = @_;

    my $dtheta = deg2rad( $theta );

    return (
        $x * cos( $dtheta ) - $y * sin( $dtheta ),
        $x * sin( $dtheta ) + $y * cos( $dtheta ) );
}

sub bound_rotated_box {
    my ( $xlen, $ylen, $theta ) = @_;

    my ( @x, @y );
    for (
	[ -$xlen / 2, -$ylen / 2 ],
	[ -$xlen / 2, $ylen / 2 ],
	[ $xlen / 2,  -$ylen / 2 ],
	[ $xlen / 2,  $ylen / 2 ] )
    {

	my ( $x, $y ) = rotate( @{$_}, $theta );
	push @x, $x;
	push @y, $y;
    }

    return (
	xmin => min( @x ),
	xmax => max( @x ),
	ymin => min( @y ),
	ymax => max( @y ) );
}

1;
